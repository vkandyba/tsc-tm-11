package ru.vkandyba.tm.api.controller;

import ru.vkandyba.tm.model.Task;

public interface ITaskController {

    void showById();

    void removeById();

    void removeByName();

    void removeByIndex();

    void updateById();

    void updateByIndex();

    void showTask(Task task);

    void showByIndex();

    void showByName();

    void showTasks();

    void clearTasks();

    void createTask();

}

