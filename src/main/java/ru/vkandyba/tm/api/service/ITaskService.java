package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Boolean existsByIndex(Integer index);

    Boolean existsById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
