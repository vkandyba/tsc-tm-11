package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Boolean existsByIndex(Integer index);

    Boolean existsById(String id);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
