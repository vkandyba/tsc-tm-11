package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
