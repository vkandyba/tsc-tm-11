package ru.vkandyba.tm.api.repository;

import ru.vkandyba.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Boolean existsByIndex(Integer index);

    Boolean existsById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
