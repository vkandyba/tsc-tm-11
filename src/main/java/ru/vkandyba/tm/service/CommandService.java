package ru.vkandyba.tm.service;

import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.api.service.ICommandService;
import ru.vkandyba.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
